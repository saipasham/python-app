FROM python:2.7
LABEL maintainer="ssaidikshit@gmail.com"
COPY . /app
WORKDIR /app
EXPOSE 8080
RUN pip install -r requirements.txt 
CMD [ "top" ]

